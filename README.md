### Patricia Campbell
Faculty at [Dawson College](https://www.dawsoncollege.qc.ca/) in Montréal

I have a long background in writing code (many languages & platforms including Linux & Android), and in writing socket libraries plus working with, designing and configuring network infrastructure.

My repos are for teaching for example [android 2019](https://gitlab.com/android518-2019)

More can be found on [github](https://github.com/campbe13) __Note  for teachers__ gitlab is a better platform for students as private
repos are possible without teacher intervention & registration proccess   With github I have to review every year-ish 
in order to maintaine an educational account which gives me private repos.
#### Extra exercises  / sources for assignments
* https://www.codility.com/  (Thanks to Cait Owens for this one)
* https://projecteuler.net/ 
* http://nifty.stanford.edu/  (interesting paper about use of exercises sites nifty [princeton.edu](https://collaborate.princeton.edu/en/publications/nifty-assignments-2))
* https://edabit.com/
* https://www.hackerrank.com/
### Technical skills
Not exhaustive, mostly relating to the content of the repos.
todo: fix the buttons colours etc
#### Coding / languages
![](https://img.shields.io/badge/Code-JavaScript-informational?style=flat&logo=JavaScript&color=61DAFB)
![](https://img.shields.io/badge/Code-Python-informational?style=flat&logo=Python&color=61DAFB)
![](https://img.shields.io/badge/Scripting-bash-informational?style=flat&logo=Linux&color=61DAFB)
![](https://img.shields.io/badge/Code-Kotlin-informational?style=flat&logo=Kotlin&color=61DAFB)
![](https://img.shields.io/badge/Code-Java-informational?style=flat&logo=Java&color=61DAFB)
![](https://img.shields.io/badge/Code-c-informational?style=flat&logo=C&color=61DAFB)
#### Platforms / Operating systems / Frameworks
![](https://img.shields.io/badge/Linux-Ubuntu-informational?style=flat&logo=Ubuntu&color=61DAFB)
![](https://img.shields.io/badge/Linux-CentOS-informational?style=flat&logo=CentOS&color=61DAFB)
![](https://img.shields.io/badge/Linux-Debian-informational?style=flat&logo=Debian&color=61DAFB)
![](https://img.shields.io/badge/Linux-RedHat-informational?style=flat&logo=RedHat&color=61DAFB)
![](https://img.shields.io/badge/Environment-Docker-informational?style=flat&logo=Docker&color=61DAFB)

![](https://img.shields.io/badge/Framework-Android-informational?style=flat&logo=Android&color=61DAFB)
![](https://img.shields.io/badge/Framework-React-informational?style=flat&logo=react&color=61DAFB)

### Current projects let's see if I keep this up to date ¯\\_(ツ)_/¯
- 🔭 I’m currently working on a Data Science course using Python and Pandas for  January 2022
### Gitlab stats
[![Anurag’s gitlab stats](https://gitlab-readme-stats.vercel.app/api?username=campbe13)](https://gitlab.com/campbe13)
<!--
[![Top Langs](https://gitlab-readme-stats.vercel.app/api/top-langs/?username=campbe13&layout=compact)](https://gitlab.com/campbe13)
**campbe13/campbe13** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
#### Sources for content
* badges [shields.io](https://shields.io/)
* created with help from [yshi95](https://yushi95.medium.com/how-to-create-a-beautiful-readme-for-your-github-profile-36957caa711c)                                           
